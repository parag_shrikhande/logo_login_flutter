import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sample_login/view/customView/customButtons.dart';

import '../firebase/auth/firebase_auth.dart';
import '../firebase/validator/validator.dart';
import 'customView/customTextField.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset : false,
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 85.0,
                        child: Text("LOGO",
                            textAlign: TextAlign.left,
                            style: style.copyWith(
                                color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 44)),
                      ),
                      SizedBox(height: 15.0),
                      customTextInput(nameController, 'Name'),
                      SizedBox(height: 15.0),
                      customTextInput(emailController, 'Email'),
                      SizedBox(height: 15.0),
                      customPasswordTextInput(passwordController, 'Password'),
                      SizedBox(height: 15.0),
                      customPasswordTextInput(confirmPasswordController, 'Confirm Password'),
                      SizedBox(
                        height: 35.0,
                      ),
                      customSignUpButton(() async {
                        if (Validator.validateEmail(email: emailController.text) != null) {
                          showAlertDialog(context, 'Invalid or Empty Email');
                        } else if (Validator.validatePassword(password: passwordController.text) != null) {
                          showAlertDialog(context, 'Incorrect Password Pattern or Empty Password');
                        } else if (Validator.validatePassword(password: confirmPasswordController.text) != null) {
                          showAlertDialog(context, 'Confirm Password Incorrect');
                        } else if (passwordController.text != confirmPasswordController.text) {
                          showAlertDialog(context, 'Password Mismatch');
                        }
                        User? user = await Firebase_Auth.registerUsingEmailPassword(
                          name: nameController.text,
                          email: emailController.text,
                          password:
                          confirmPasswordController.text,
                        );
                      }, context)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );;
  }

  showAlertDialog(BuildContext context, String alertMessage) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(''),
          content: Text(alertMessage),
          actions: [
            okButton,
          ],
        );
      },
    );
  }
}
