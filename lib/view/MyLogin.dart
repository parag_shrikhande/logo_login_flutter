import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sample_login/view/customView/customButtons.dart';
import 'package:sample_login/view/customView/customTextField.dart';

import '../firebase/auth/firebase_auth.dart';
import '../firebase/validator/validator.dart';
import '../view model/MyLoginVM.dart';
import 'SignUp.dart';

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  State<MyLogin> createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  showAlertDialog(BuildContext context, String alertMessage) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(''),
          content: Text(alertMessage),
          actions: [
            okButton,
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset : false,
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 85.0,
                        child: Text("LOGO",
                            textAlign: TextAlign.center,
                            style: style.copyWith(
                                color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 44)),
                      ),
                      SizedBox(height: 45.0),
                      customEmailTextInput(emailController),
                      SizedBox(height: 25.0),
                      customPasswordTextInput(passwordController, 'Password'),
                      SizedBox(
                        height: 35.0,
                      ),
                      customLoginButton(() async {
                        if (Validator.validateEmail(email: emailController.text) != null) {
                          showAlertDialog(context, 'Invalid or Empty Email');
                        } else if (Validator.validatePassword(password: passwordController.text) != null) {
                          showAlertDialog(context, 'Incorrect Password Pattern or Empty Password');
                        }
                        User? user = await Firebase_Auth.signInUsingEmailPassword(
                          email: emailController.text,
                          password: passwordController.text, context: context,
                        );
                        if (user != null) {
                          showAlertDialog(context, 'Login Success');;
                        }
                      }, context),
                    ],
                  ),
                ),
              ),
              Row(
                children: [
                  customButton(() {
                    // TBD
                  }, 'Forgot Password?'),
                  Spacer(),
                  customButton(() {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => SignUp()),
                    );
                  }, 'Signup')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}