import 'package:flutter/cupertino.dart';
import '../firebase/validator/validator.dart';

class SignUpVM with ChangeNotifier {
  bool isValidEmail(String email) {
    return Validator.validateEmail(email: email) != null;
  }

  bool isValidPassword(String email) {
    return Validator.validateEmail(email: email) != null;
  }
}