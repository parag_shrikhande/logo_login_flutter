import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget customPasswordTextInput(TextEditingController _textEditingController, String hintText) {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  return TextField(
    controller: _textEditingController,
    obscureText: true,
    style: style,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: hintText,
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
}

Widget customEmailTextInput(TextEditingController _textEditingController) {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  return TextField(
    controller: _textEditingController,
    obscureText: false,
    style: style,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Email",
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
}

Widget customTextInput(TextEditingController _textEditingController, String hintText) {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  return TextField(
    controller: _textEditingController,
    obscureText: false,
    style: style,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: hintText,
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );
}